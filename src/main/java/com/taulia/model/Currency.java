package com.taulia.model;

public enum Currency {
    USD("USD"),
    JSD("JSD"),
    CAD("CAD");

    private String currency;

    Currency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }
}
