package com.taulia.model;

import java.util.Objects;

public class Buyer {
    private String name;
    private String imageName;
    private String encodedImage;
    private String invoiceDueDate;
    private String invoiceNumber;
    private Double amount;
    private Currency currency;
    private InvoiceStatus status;
    private String supplier;

    public Buyer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getEncodedImage() {
        return encodedImage;
    }

    public void setEncodedImage(String encodedImage) {
        this.encodedImage = encodedImage;
    }

    public String getInvoiceDueDate() {
        return invoiceDueDate;
    }

    public void setInvoiceDueDate(String invoiceDueDate) {
        this.invoiceDueDate = invoiceDueDate;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public InvoiceStatus getStatus() {
        return status;
    }

    public void setStatus(InvoiceStatus status) {
        this.status = status;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Buyer buyer = (Buyer) o;
        return Objects.equals(name, buyer.name) &&
                Objects.equals(imageName, buyer.imageName) &&
                Objects.equals(encodedImage, buyer.encodedImage) &&
                Objects.equals(invoiceDueDate, buyer.invoiceDueDate) &&
                Objects.equals(invoiceNumber, buyer.invoiceNumber) &&
                Objects.equals(amount, buyer.amount) &&
                currency == buyer.currency &&
                status == buyer.status &&
                Objects.equals(supplier, buyer.supplier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, imageName, encodedImage, invoiceDueDate, invoiceNumber, amount, currency, status, supplier);
    }

    @Override
    public String toString() {
        return "Buyer{" +
                "name='" + name + '\'' +
                ", imageName='" + imageName + '\'' +
                ", encodedImage='" + encodedImage + '\'' +
                ", invoiceDueDate='" + invoiceDueDate + '\'' +
                ", invoiceNumber='" + invoiceNumber + '\'' +
                ", amount=" + amount +
                ", currency=" + currency +
                ", status=" + status +
                ", supplier='" + supplier + '\'' +
                '}';
    }
}
