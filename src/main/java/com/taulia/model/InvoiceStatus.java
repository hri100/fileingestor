package com.taulia.model;

public enum InvoiceStatus {
 NEW("NEW"),
 VOID("VOID"),
 PAID("PAID");

 private String status;

    InvoiceStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}