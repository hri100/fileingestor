package com.taulia.file.ingestor;

import static java.lang.String.format;

public enum FileFormat {
    CSV("csv"),
    XML("xml");

    private String format;

    FileFormat(String format) {
        this.format = format;
    }

    public String getFormat() {
        return format;
    }

    public static FileFormat fromString(String fileExtension) {
        for(FileFormat format : FileFormat.values()) {
            if (format.getFormat().equalsIgnoreCase(fileExtension)) {
                return format;
            }
        }
        throw new IllegalArgumentException(format("Unsupported file format %s", fileExtension));
    }
}
