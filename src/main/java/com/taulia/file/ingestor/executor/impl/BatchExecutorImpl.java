package com.taulia.file.ingestor.executor.impl;

import com.taulia.file.ingestor.FileFormat;
import com.taulia.file.ingestor.output.processor.BuyerOutputProcessorFactory;
import com.taulia.file.ingestor.output.processor.data.BuyerOutputDataProcessor;
import com.taulia.file.ingestor.executor.BatchExecutor;
import com.taulia.file.ingestor.output.producers.OutputProducer;
import com.taulia.model.Buyer;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class BatchExecutorImpl implements BatchExecutor {
    private static final int THREAD_POOL_SIZE = 10;

    private ExecutorService executorService;

    public BatchExecutorImpl() {
        this.executorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
    }

    @Override
    public void processBatch(List<Buyer> buyers, FileFormat[] formatConverters) {
        List<BuyerOutputDataProcessor> converters = getConvertersFrom(formatConverters);
        executorService.execute(new OutputProducer(buyers, converters));
        executorService.shutdown();
    }

    private List<BuyerOutputDataProcessor> getConvertersFrom(FileFormat[] formatConverters) {
        List<BuyerOutputDataProcessor> converters = new LinkedList<>();
        for (FileFormat converterFormat : formatConverters) {
            converters.add(BuyerOutputProcessorFactory.getInStanceOf(converterFormat));
        }
        return converters;
    }
}
