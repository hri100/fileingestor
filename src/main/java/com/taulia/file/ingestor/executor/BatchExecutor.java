package com.taulia.file.ingestor.executor;

import com.taulia.file.ingestor.FileFormat;
import com.taulia.model.Buyer;

import java.util.List;

public interface BatchExecutor {
    void processBatch(List<Buyer> buyers, FileFormat[] outputFormats);
}
