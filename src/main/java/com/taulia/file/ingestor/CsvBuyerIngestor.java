package com.taulia.file.ingestor;

import com.taulia.file.ingestor.executor.BatchExecutor;
import com.taulia.model.Buyer;
import com.taulia.model.Currency;
import com.taulia.model.InvoiceStatus;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static com.taulia.file.ingestor.output.processor.data.impl.CsvHeaders.*;
import static java.lang.String.format;

public class CsvBuyerIngestor {

    private static final int BATCH_SIZE = 100;

    private String filePath;
    private BatchExecutor executor;

    public CsvBuyerIngestor(String filePath, BatchExecutor executor) {
        validateFile(filePath);
        this.filePath = filePath;
        this.executor = executor;
    }

    private void validateFile(String filePath) {
        File file = new File(filePath);
        if (!file.exists()) {
            throw new IllegalArgumentException(format("File %s does not exist",filePath));
        }
    }


    public void ingest(FileFormat[] converters) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath));
             CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader(HEADERS)
                     .withIgnoreHeaderCase().withFirstRecordAsHeader())) {
            List<CSVRecord> data = readBatch(parser);
            List<Buyer> buyers = parseData(data);
            executor.processBatch(buyers, converters);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<CSVRecord> readBatch(CSVParser parser) {
        Iterator<CSVRecord> iterator = parser.iterator();
        List<CSVRecord> data = new LinkedList<>();
        int i = 0;
        while (i < BATCH_SIZE && iterator.hasNext()) {
            CSVRecord record = iterator.next();
            data.add(record);
            i++;
        }
        return data;
    }

    private List<Buyer> parseData(List<CSVRecord> data) {
        List<Buyer> buyers = new LinkedList<>();
        for (CSVRecord record : data) {
            Buyer buyer = parseDataEntry(record);
            buyers.add(buyer);
        }
        return buyers;
    }

    private Buyer parseDataEntry(CSVRecord record) {
        Buyer buyer = new Buyer((String) getIfExists(record, BUYER_INDEX));
        buyer.setImageName((String) getIfExists(record, IMAGE_NAME_INDEX));
        buyer.setEncodedImage((String) getIfExists(record,ENCODED_IMAGE_INDEX));
        buyer.setInvoiceDueDate((String) getIfExists(record, INVOICE_DUE_DATE_INDEX));
        buyer.setInvoiceNumber((String) getIfExists(record, INVOICE_NUMBER_INDEX));
        buyer.setAmount(getIfExists(record, INVOICE_AMOUNT_INDEX) == null ? null : Double.parseDouble(record.get(INVOICE_AMOUNT_INDEX)));
        buyer.setCurrency(getIfExists(record, INVOICE_CURRENCY_INDEX) == null ? null : Currency.valueOf(record.get(INVOICE_CURRENCY_INDEX)));
        buyer.setStatus(getIfExists(record, INVOICE_STATUS_INDEX) == null ? null : InvoiceStatus.valueOf(record.get(INVOICE_STATUS_INDEX)));
        buyer.setSupplier((String) getIfExists(record, SUPPLIER_INDEX));
        return buyer;
    }

    private Object getIfExists(CSVRecord record, int header) {
        if (record.isSet(header) && record.get(header).length() != 0) {
            return record.get(header);
        }
        return null;
    }
}
