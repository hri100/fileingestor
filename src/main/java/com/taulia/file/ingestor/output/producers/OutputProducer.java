package com.taulia.file.ingestor.output.producers;

import com.taulia.file.ingestor.output.processor.data.BuyerOutputDataProcessor;
import com.taulia.model.Buyer;

import java.util.List;

public class OutputProducer implements Runnable {
    private List<Buyer> buyers;
    private List<BuyerOutputDataProcessor> converters;

    public OutputProducer(List<Buyer> buyers, List<BuyerOutputDataProcessor> converters) {
        this.buyers = buyers;
        this.converters = converters;
    }

    @Override
    public void run() {
        for (BuyerOutputDataProcessor outputFormat : converters) {
            for (Buyer buyer: buyers) {
                outputFormat.save(buyer);
            }
        }
    }
}
