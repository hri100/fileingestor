package com.taulia.file.ingestor.output.processor.data.impl;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.taulia.file.ingestor.output.processor.data.BuyerOutputDataProcessor;
import com.taulia.model.Buyer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static java.lang.String.format;

public class XmlBuyerOutputDataProcessor implements BuyerOutputDataProcessor {
    private String destination;

    public XmlBuyerOutputDataProcessor(String destination) {
        this.destination = destination;
    }

    @Override
    public File save(Buyer buyer) {
        validateBuyer(buyer);
        File file = new File(destination + buyer.getInvoiceDueDate() + "/" + buyer.getInvoiceNumber() + "/" + buyer.getName() + ".xml");
        try {
            String xml = convertToXml(buyer);
            Files.createDirectories(Paths.get(destination + buyer.getInvoiceDueDate() + "/" + buyer.getInvoiceNumber() + "/"));
            saveFile(xml, file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    private void validateBuyer(Buyer buyer) {
        if (buyer == null || isNullOrEmpty(buyer.getName()) || isNullOrEmpty(buyer.getInvoiceDueDate()) || isNullOrEmpty(buyer.getInvoiceNumber())) {
            throw new IllegalArgumentException(format("Invalid record %s", buyer));
        }
    }

    private boolean isNullOrEmpty(String str) {
        return str == null || str.length() == 0;
    }

    private String convertToXml(Buyer buyer) throws JsonProcessingException {
        XmlMapper mapper = getMapper();
        buyer.setEncodedImage(null);
        return mapper.writeValueAsString(buyer);
    }

    private XmlMapper getMapper() {
        XmlMapper mapper = new XmlMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper;
    }

    private void saveFile(String xml, File file) throws IOException {
        try(FileWriter writer = new FileWriter(file)) {
            writer.write(xml);
        }
    }
}