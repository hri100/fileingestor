package com.taulia.file.ingestor.output.processor;

import com.taulia.file.ingestor.FileFormat;
import com.taulia.file.ingestor.output.processor.data.BuyerOutputDataProcessor;
import com.taulia.file.ingestor.output.processor.data.impl.CsvBuyerOutputDataProcessor;
import com.taulia.file.ingestor.output.processor.data.impl.XmlBuyerOutputDataProcessor;

import static com.taulia.file.ingestor.FileFormat.CSV;
import static com.taulia.file.ingestor.FileFormat.XML;
import static java.lang.String.format;

public class BuyerOutputProcessorFactory {
    private static final String MAIN_TARGET_DIRECTORY = "src/main/resources/";
    public static final String TEST_TARGET_DIRECTORY = "src/test/resources/";

    public static BuyerOutputDataProcessor getInStanceOf(FileFormat format) {
        String environment = System.getenv("environment");
        String directory = getTargetDirectory(environment);
        if (CSV == format) {
            return new CsvBuyerOutputDataProcessor(directory + CSV.toString() + "/");
        } else if (XML == format) {
            return new XmlBuyerOutputDataProcessor(directory + XML.toString() + "/");
        } else {
            throw new IllegalArgumentException(format("Unsupported output format %s", format));
        }
    }

    private static String getTargetDirectory(String environment) {
        if("test".equalsIgnoreCase(environment)) {
            return TEST_TARGET_DIRECTORY;
        }
        return MAIN_TARGET_DIRECTORY;
    }
}
