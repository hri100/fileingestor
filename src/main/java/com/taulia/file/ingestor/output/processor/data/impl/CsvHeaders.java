package com.taulia.file.ingestor.output.processor.data.impl;

public final class CsvHeaders {

    public static final String[] HEADERS = {"buyer", "invoice_name", "invoice_image", "invoice_due_date",
            "invoice_number", "invoice_amount", "invoice_currency", "invoice_status", "supplier"};
    public static final int BUYER_INDEX = 0;
    public static final int IMAGE_NAME_INDEX = 1;
    public static final int ENCODED_IMAGE_INDEX = 2;
    public static final int INVOICE_DUE_DATE_INDEX = 3;
    public static final int INVOICE_NUMBER_INDEX = 4;
    public static final int INVOICE_AMOUNT_INDEX = 5;
    public static final int INVOICE_CURRENCY_INDEX = 6;
    public static final int INVOICE_STATUS_INDEX = 7;
    public static final int SUPPLIER_INDEX = 8;

    private CsvHeaders() {}
}
