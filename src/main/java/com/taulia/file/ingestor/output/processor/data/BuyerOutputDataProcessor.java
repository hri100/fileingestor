package com.taulia.file.ingestor.output.processor.data;

import com.taulia.model.Buyer;

import java.io.File;

public interface BuyerOutputDataProcessor {
    File save(Buyer buyer);
}
