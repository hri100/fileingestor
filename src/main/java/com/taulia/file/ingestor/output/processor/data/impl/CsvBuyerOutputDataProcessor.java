package com.taulia.file.ingestor.output.processor.data.impl;

import com.taulia.file.ingestor.output.processor.data.BuyerOutputDataProcessor;
import com.taulia.model.Buyer;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import static com.taulia.file.ingestor.output.processor.data.impl.CsvHeaders.HEADERS;
import static java.lang.String.format;

public class CsvBuyerOutputDataProcessor implements BuyerOutputDataProcessor {

    private String destination;

    public CsvBuyerOutputDataProcessor(String destination) {
        this.destination = destination;
    }

    @Override
    public File save(Buyer buyer) {
        validateBuyer(buyer);
        File file = new File(destination + buyer.getName());
        try (CSVPrinter printer = getCsvPrinter(file)) {
            printer.printRecord(buyer.getName(), buyer.getImageName(), buyer.getEncodedImage(),
                    buyer.getInvoiceDueDate(), buyer.getInvoiceNumber(), buyer.getAmount(),
                    buyer.getCurrency(), buyer.getStatus(), buyer.getSupplier());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    private CSVPrinter getCsvPrinter(File file) throws IOException {
        String path = file.getPath();
        Files.createDirectories(Paths.get(destination));
        if (file.exists()) {
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(path), StandardOpenOption.APPEND);
            return new CSVPrinter(writer, CSVFormat.DEFAULT);
        }
        return new CSVPrinter(Files.newBufferedWriter(Paths.get(path)), CSVFormat.DEFAULT.withHeader(HEADERS));
    }

    private void validateBuyer(Buyer buyer) {
        if (buyer == null || buyer.getName() == null || buyer.getName().length() == 0) {
            throw new IllegalArgumentException(format("Invalid record %s", buyer));
        }
    }
}
