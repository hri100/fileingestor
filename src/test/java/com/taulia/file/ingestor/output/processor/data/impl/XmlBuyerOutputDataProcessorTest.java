package com.taulia.file.ingestor.output.processor.data.impl;

import com.taulia.file.ingestor.output.processor.data.BuyerOutputDataProcessor;
import com.taulia.model.Buyer;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class XmlBuyerOutputDataProcessorTest {

    private static final String TEST_DESTINATION = "src/test/resources/xml/";
    private BuyerOutputDataProcessor buyerOutputDataProcessor;

    @BeforeAll
    public static void initializeDirectories() throws IOException {
        Files.createDirectories(Paths.get(TEST_DESTINATION));
    }

    @BeforeEach
    public void setUp() {
        buyerOutputDataProcessor = new XmlBuyerOutputDataProcessor(TEST_DESTINATION);
    }

    @AfterEach
    void tearDown() throws IOException {
        FileUtils.cleanDirectory(new File(TEST_DESTINATION));
    }

    @Test
    public void testCreateXmlFromBuyer() throws IOException {
        Buyer buyer = new Buyer("TestName");
        buyer.setInvoiceDueDate("2020-08-22");
        buyer.setInvoiceNumber("TestNumber");

        File actual = buyerOutputDataProcessor.save(buyer);
        File expected = new File(TEST_DESTINATION + buyer.getInvoiceDueDate() + "/" + buyer.getInvoiceNumber() + "/" + buyer.getName() + ".xml");

        assertTrue(expected.exists());
        assertTrue(FileUtils.contentEquals(expected, actual));
        assertEquals("<Buyer><name>TestName</name><invoiceDueDate>2020-08-22</invoiceDueDate><invoiceNumber>TestNumber</invoiceNumber></Buyer>",
                readFrom(expected));
    }

    @Test
    public void testCreateXmlFromBuyerWithIncompleteData() throws IOException {
        Buyer buyer = new Buyer("TestName");
        buyer.setInvoiceDueDate("2020-08-22");
        buyer.setInvoiceNumber("TestNumber");

        File actual = buyerOutputDataProcessor.save(buyer);
        File expected = new File(TEST_DESTINATION + buyer.getInvoiceDueDate() + "/" + buyer.getInvoiceNumber() + "/" + buyer.getName() + ".xml");

        assertTrue(expected.exists());
        assertTrue(FileUtils.contentEquals(expected, actual));
        assertEquals("<Buyer><name>TestName</name><invoiceDueDate>2020-08-22</invoiceDueDate><invoiceNumber>TestNumber</invoiceNumber></Buyer>",
                readFrom(expected));
    }

    @Test
    public void testSaveInvalidBuyer() {
        Buyer buyer = new Buyer(null);

        Exception e = assertThrows(IllegalArgumentException.class, () -> {
            buyerOutputDataProcessor.save(buyer);
        });

        assertEquals("Invalid record Buyer{name='null', imageName='null', encodedImage='null', invoiceDueDate='null', invoiceNumber='null', amount=null, currency=null, status=null, supplier='null'}"
                , e.getMessage());
    }

    private String readFrom(File file) throws IOException {
        StringBuilder builder = new StringBuilder();
        try(BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while((line = reader.readLine()) != null) {
                builder.append(line);
            }
        }
        return builder.toString();
    }
}