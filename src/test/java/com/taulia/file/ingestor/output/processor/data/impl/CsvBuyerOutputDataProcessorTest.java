package com.taulia.file.ingestor.output.processor.data.impl;

import com.taulia.file.ingestor.output.processor.data.BuyerOutputDataProcessor;
import com.taulia.model.Buyer;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CsvBuyerOutputDataProcessorTest {

    private BuyerOutputDataProcessor buyerOutputDataProcessor;
    private static final String TEST_DESTINATION = "src/test/resources/csv/";

    @BeforeAll
    public static void initializeDirectories() throws IOException {
        Files.createDirectories(Paths.get(TEST_DESTINATION));
    }

    @BeforeEach
    public void setUp() {
        buyerOutputDataProcessor = new CsvBuyerOutputDataProcessor(TEST_DESTINATION);
    }

    @AfterEach
    void tearDown() throws IOException {
        FileUtils.cleanDirectory(new File(TEST_DESTINATION));
    }

    @Test
    public void testCreateCsvFromBuyer() throws IOException {
        Buyer buyer = new Buyer("Fanta");

        File actual = buyerOutputDataProcessor.save(buyer);
        File expected = new File(TEST_DESTINATION + buyer.getName());
        List<Buyer> buyers = CsvReaderUtil.getContentFrom(TEST_DESTINATION + buyer.getName());

        assertTrue(actual.exists());
        assertTrue(FileUtils.contentEquals(expected, actual));
        assertEquals(buyer, buyers.get(0));
    }

    @Test
    public void testCreateCsvFromBuyerWithIncompleteData() throws IOException {
        Buyer buyer = new Buyer("Pepsi");

        File actual = buyerOutputDataProcessor.save(buyer);
        File expected = new File(TEST_DESTINATION + buyer.getName());
        List<Buyer> buyers = CsvReaderUtil.getContentFrom(TEST_DESTINATION + buyer.getName());


        assertTrue(actual.exists());
        assertTrue(FileUtils.contentEquals(expected, actual));
        assertEquals(buyer, buyers.get(0));

    }

    @Test
    public void testSaveInvalidBuyer() {
        Buyer buyer = new Buyer("");

        Exception e = assertThrows(IllegalArgumentException.class, () -> {
            buyerOutputDataProcessor.save(buyer);

        });

        assertEquals("Invalid record Buyer{name='', imageName='null', encodedImage='null', invoiceDueDate='null', invoiceNumber='null', amount=null, currency=null, status=null, supplier='null'}", e.getMessage());
    }

    @Test
    public void testAddNewBuyerToExistingCsv() throws IOException {
        Buyer pepsi1 = new Buyer("Pepsi");
        Buyer pepsi2 = new Buyer("Pepsi");
        buyerOutputDataProcessor.save(pepsi1);
        File expected = buyerOutputDataProcessor.save(pepsi2);

        File actual = new File(TEST_DESTINATION + pepsi1.getName());
        List<Buyer> buyers = CsvReaderUtil.getContentFrom(TEST_DESTINATION + pepsi1.getName());

        assertTrue(actual.exists());
        assertTrue(FileUtils.contentEquals(expected, actual));
        assertEquals(pepsi1, buyers.get(0));
        assertEquals(pepsi2, buyers.get(1));
    }
}