package com.taulia.file.ingestor.output.processor.data.impl;

import com.taulia.model.Buyer;
import com.taulia.model.Currency;
import com.taulia.model.InvoiceStatus;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.taulia.file.ingestor.output.processor.data.impl.CsvHeaders.*;


public class CsvReaderUtil {


    public static List<Buyer> getContentFrom(String filePath) throws IOException {
        List<Buyer> buyers = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath));
             CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT
                     .withHeader(HEADERS).withIgnoreHeaderCase().withFirstRecordAsHeader())) {
            for (CSVRecord record : parser) {
                Buyer buyer = new Buyer((String) getIfExists(record,BUYER_INDEX));
                buyer.setImageName((String) getIfExists(record, IMAGE_NAME_INDEX));
                buyer.setEncodedImage((String) getIfExists(record,ENCODED_IMAGE_INDEX));
                buyer.setInvoiceDueDate((String) getIfExists(record, INVOICE_DUE_DATE_INDEX));
                buyer.setInvoiceNumber((String) getIfExists(record, INVOICE_NUMBER_INDEX));
                buyer.setAmount(getIfExists(record, INVOICE_AMOUNT_INDEX) == null ? null : Double.parseDouble(record.get(INVOICE_AMOUNT_INDEX)));
                buyer.setCurrency(getIfExists(record, INVOICE_CURRENCY_INDEX) == null ? null : Currency.valueOf(record.get(INVOICE_CURRENCY_INDEX)));
                buyer.setStatus(getIfExists(record, INVOICE_STATUS_INDEX) == null ? null : InvoiceStatus.valueOf(record.get(INVOICE_STATUS_INDEX)));
                buyer.setSupplier((String) getIfExists(record, SUPPLIER_INDEX));
                buyers.add(buyer);
            }
        }
        return buyers;
    }

    private static Object getIfExists(CSVRecord record, int header) {
        if (record.isSet(header) && record.get(header).length() != 0) {
            return record.get(header);
        }
        return null;
    }
}
