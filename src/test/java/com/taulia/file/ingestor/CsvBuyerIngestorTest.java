package com.taulia.file.ingestor;

import com.taulia.file.ingestor.executor.BatchExecutor;
import com.taulia.file.ingestor.executor.impl.BatchExecutorImpl;
import com.taulia.file.ingestor.output.processor.data.impl.CsvReaderUtil;
import com.taulia.model.Buyer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CsvBuyerIngestorTest {

    public static final String TEST_DATA = "src/test/resources/dev_task_data.csv";
    private CsvBuyerIngestor ingestor;
    private FileFormat[] actualFormats;
    private  List<Buyer> actualBuyers;

    @BeforeEach
    public void setUp() {
        ingestor = new CsvBuyerIngestor(TEST_DATA, (buyers, outputFormats) -> {
            actualBuyers = buyers;
            actualFormats = outputFormats;
        });
    }

    @Test
    public void testCorrectParametersAreGivenToTheExecutor() throws IOException {
        FileFormat[] expectedFormats = new FileFormat[]{FileFormat.XML, FileFormat.CSV};
        List<Buyer> expectedBuyers = CsvReaderUtil.getContentFrom(TEST_DATA);
        ingestor.ingest(expectedFormats);

        assertEquals(expectedFormats, actualFormats);
        assertEquals(expectedBuyers, actualBuyers);
    }

    @Test
    public void testIngestNonExistentFile() {
        Exception e = assertThrows(IllegalArgumentException.class, () -> {
                ingestor = new CsvBuyerIngestor("non-existent-file", (buyers, outputFormats) -> {
                    actualBuyers = buyers;
                    actualFormats = outputFormats;
                });
        });

        assertEquals("File non-existent-file does not exist", e.getMessage());
    }

}