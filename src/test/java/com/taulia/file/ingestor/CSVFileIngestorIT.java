package com.taulia.file.ingestor;

import com.taulia.file.ingestor.executor.BatchExecutor;
import com.taulia.file.ingestor.executor.impl.BatchExecutorImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class CSVFileIngestorIT {
    private static final String TEST_DATA = "src/test/resources/dev_task_data.csv";
    private static final String CSV_TEST_DIRECTORY = "src/test/resources/CSV/";
    private static final String XML_TEST_DIRECTORY = "src/test/resources/XML/";
    private CsvBuyerIngestor ingestor;

    @BeforeEach
    public void setUp() {
        ingestor = new CsvBuyerIngestor(TEST_DATA, new BatchExecutorImpl());
    }


    @Test
    public void testCreateCsvOutputFiles() throws InterruptedException {
        BatchExecutor executor = new BatchExecutorImpl();
        ingestor = new CsvBuyerIngestor(TEST_DATA, executor);
        ingestor.ingest(new FileFormat[] {FileFormat.CSV});

        Thread.sleep(1000l);

        assertTrue(new File(CSV_TEST_DIRECTORY + "Axtronics").exists());
        assertTrue(new File(CSV_TEST_DIRECTORY + "South African Gold Mines Corp").exists());
        assertTrue(new File(CSV_TEST_DIRECTORY + "Traksas").exists());
    }

    @Test
    public void testCreateXMlOutputFiles() throws InterruptedException {
        BatchExecutor executor = new BatchExecutorImpl();
        ingestor = new CsvBuyerIngestor(TEST_DATA, executor);
        ingestor.ingest(new FileFormat[] {FileFormat.XML});

        Thread.sleep(1000l);

        assertTrue(new File(XML_TEST_DIRECTORY + "2019-11-13/AA16789-4/South African Gold Mines Corp.xml").exists());
        assertTrue(new File(XML_TEST_DIRECTORY + "2019-12-13/AA16789-4/South African Gold Mines Corp.xml").exists());
        assertTrue(new File(XML_TEST_DIRECTORY + "2020-01-01/PPOO90/Traksas.xml").exists());
        assertTrue(new File(XML_TEST_DIRECTORY + "2020-02-13/AA16789-4/South African Gold Mines Corp.xml").exists());
        assertTrue(new File(XML_TEST_DIRECTORY + "2020-09-01/AA16789-1/South African Gold Mines Corp.xml").exists());
        assertTrue(new File(XML_TEST_DIRECTORY + "2020-09-01/AA16789-2/South African Gold Mines Corp.xml").exists());
        assertTrue(new File(XML_TEST_DIRECTORY + "2020-09-01/AA16789-3/South African Gold Mines Corp.xml").exists());
        assertTrue(new File(XML_TEST_DIRECTORY + "2020-09-01/IIPT78-65/Traksas.xml").exists());
        assertTrue(new File(XML_TEST_DIRECTORY + "2020-10-23/AA-C56790/Axtronics.xml").exists());
        assertTrue(new File(XML_TEST_DIRECTORY + "2020-10-23/PP7890U/Traksas.xml").exists());


    }
}
