## Prerequisites
* Java 8

## Tests 
In order to run the `CSVFileIngestorIT` you must set the environmen variable `environment` to `test`

## Solution
The implemented solution follows the following algorithm:
* Read the initial `csv` file in chunks
* For every chunk start a new thread and process the chunk and generate the requested output formats

## Assumptions
* The input `csv` file will be sorted by the `buyer` header
* The duplicate records in the csv file will be less than the chunk size